package rmi;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import rmi.custom.CustomRmiAbstractFactory;

public class CustomRMITest {

  @Rule
  public Timeout timeout = new Timeout(1000);

  @Test
  public void shouldReturnStubToInvokeRemoteMethod() {
    final CustomRmiAbstractFactory factory = new CustomRmiAbstractFactory(3000);
    Thread server = null;
    try {
      final Skeleton skeleton = factory.createSkeleton();
      final HelloWorld stub = skeleton.exportObject(new HelloWorld());
      server = new Thread(() -> {
        try {
          skeleton.start();
        } catch (Exception e) {
          e.printStackTrace();
        }
      });
      server.start();

      Assert.assertEquals("Hello, World", stub.hello("World"));
    } finally {
      if (server != null) {
        server.stop();
      }
    }
  }

  @Test
  public void shouldInvokeCorrectObject() {
    final CustomRmiAbstractFactory factory = new CustomRmiAbstractFactory(3001);
    Thread server = null;
    try {
      final Skeleton skeleton = factory.createSkeleton();
      final HelloWorld helloStub = skeleton.exportObject(new HelloWorld());
      final GoodByeWorld goodByeStub = skeleton.exportObject(new GoodByeWorld());
      server = new Thread(() -> {
        try {
          skeleton.start();
        } catch (Exception e) {
          e.printStackTrace();
        }
      });
      server.start();

      Assert.assertEquals("Good bye, World", goodByeStub.goodBye("World"));
      Assert.assertEquals("Hello, World", helloStub.hello("World"));

    } finally {
      if (server != null) {
        server.stop();
      }
    }
  }

  @Test
  public void shouldTransportCorrectException() {
    final CustomRmiAbstractFactory factory = new CustomRmiAbstractFactory(3002);
    Thread server = null;
    try {
      final Skeleton skeleton = factory.createSkeleton();
      final Throw stub = skeleton.exportObject(new Throw());
      server = new Thread(() -> {
        try {
          skeleton.start();
        } catch (Exception e) {
          e.printStackTrace();
        }
      });
      server.start();

      try {
        stub.throwMessage("World");
        Assert.fail("should throw exception");
      } catch (IllegalArgumentException e) {
        Assert.assertEquals("Throw World", e.getMessage());
      } catch (Exception e) {
        e.printStackTrace();
        Assert.fail("should throw exception");
      }

    } finally {
      if (server != null) {
        server.stop();
      }
    }
  }

  public static class HelloWorld {

    public String hello(String name) {
      return "Hello, " + name;
    }
  }

  public static class GoodByeWorld {

    public String goodBye(String name) {
      return "Good bye, " + name;
    }
  }

  public static class Throw {

    public String throwMessage(String name) {
      throw new IllegalArgumentException("Throw " + name);
    }
  }
}