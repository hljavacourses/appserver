package rmi.custom;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;
import rmi.RmiRegistry;
import rmi.Skeleton;

public class LocalRegistryTest {

  @Test
  @Ignore
  public void clientRegistryShouldBindInLocal() {
    final CustomRmiAbstractFactory factory = new CustomRmiAbstractFactory(0, 3005);
    new Thread(() -> {
      try {
        final Registry registryClient = factory.createRegistry();
        registryClient.start();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }).start();

    final Skeleton skeleton = factory.createSkeleton();
    factory.getRegistry().bind("test", skeleton.exportObject("value"));

    assertEquals("value", factory.getRegistry().lookup("test"));

    factory.getRegistry().rebind("test", skeleton.exportObject("value"));
    assertEquals("next value", factory.getRegistry().lookup("test"));
  }
}