package rmi.multiprocess;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;
import org.junit.Test;

public class InterProcessRmiTest {

  @Test
  public void shouldWorkInDifferentProcesses() throws IOException, InterruptedException {
    final Thread server = new Thread(Server::main);
    final ProcessBuilder clientBuilder = new ProcessBuilder().redirectErrorStream(true)
        .command("java", "-cp", System.getProperty("java.class.path"),
            Client.class.getCanonicalName());
    try {
      server.start();
      final Process client = clientBuilder.start();
      final BufferedReader reader = new BufferedReader(
          new InputStreamReader(client.getInputStream()));
      final String actual = reader.lines().collect(Collectors.joining("\n"));
      assertEquals("Hello, World", actual);
      client.waitFor();
    } finally {
      server.interrupt();
    }
  }

}
