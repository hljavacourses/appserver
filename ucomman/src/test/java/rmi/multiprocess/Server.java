package rmi.multiprocess;

import rmi.RmiRegistry;
import rmi.Skeleton;
import rmi.custom.CustomRmiAbstractFactory;
import rmi.custom.Registry;
import rmi.custom.WithStubDescriptor;

public class Server {

  public static void main(String... args) {
    final CustomRmiAbstractFactory factory = new CustomRmiAbstractFactory(8080, 8081);
    final Skeleton skeleton = factory.createSkeleton();
    final WithStubDescriptor stub = skeleton.exportObject(new ServerObject());
    new Thread(() -> {
      final Registry localRegistry = factory.createRegistry();
      localRegistry.bind("serverObject", stub.stubDescriptor());
      localRegistry.start();
    }).start();
    // Поток обработки реальной команды
    new Thread(() -> {
      try {
        skeleton.start();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }).start();
  }
}
