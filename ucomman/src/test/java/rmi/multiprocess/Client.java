package rmi.multiprocess;

import rmi.RmiRegistry;
import rmi.custom.CustomRmiAbstractFactory;
import rmi.custom.RegistryClient;

public class Client {

  public static void main(String[] args) {
    final CustomRmiAbstractFactory factory = new CustomRmiAbstractFactory(0, 8081);
    final RmiRegistry registry = factory.getRegistry();
    final ServerObject stub = (ServerObject) registry.lookup("serverObject", ServerObject.class);
    System.out.println(stub.hello("World"));
  }
}
