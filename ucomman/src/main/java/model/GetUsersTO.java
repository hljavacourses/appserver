package model;

import java.util.ArrayList;
import java.util.List;

public class GetUsersTO implements TransferObject {

  private List<UserTO> users = new ArrayList<>();

  public GetUsersTO(List<UserTO> users) {
    this.users = users;
  }

  public GetUsersTO() {

  }

  public List<UserTO> getUsers() {
    return users;
  }
}
