package model.market;

import model.Entity;

import java.io.Serializable;
import java.rmi.Remote;

public class Stock extends Entity implements Serializable, Remote, StockTO {

    private static final long serialVersionUID = -2949743093336158241L;

    private String symbol;
    private Double price;

    public Stock() {
        super();
    }

    public Stock(final Long id) {
        super(id);
    }

    public Stock(Long id, String ssn, String symbol, Double price) {
        super(id);
        this.symbol = symbol;
        this.price = price;
    }

    @Override
    public String getSymbol() {
        return symbol;
    }

    @Override
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public Double getPrice() {
        return price;
    }

    @Override
    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Stock stock = (Stock) o;

        if (symbol != null ? !symbol.equals(stock.symbol) : stock.symbol != null) return false;
        return price != null ? price.equals(stock.price) : stock.price == null;
    }

    @Override
    public String toString() {
        return "Stock(super=" + super.toString() + ", symbol=" + this.getSymbol() + ", price=" + this
                .getPrice() + ")";
    }

}
