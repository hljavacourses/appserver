package model.market;

import model.TransferObject;

public interface CustomerTO extends TransferObject {

    String getSsn();

    void setSsn(String ssn);

    String getName();

    void setName(String name);

    String getAddress();

    void setAddress(String address);

}
