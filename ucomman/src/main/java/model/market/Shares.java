package model.market;

import model.Entity;

import java.io.Serializable;
import java.rmi.Remote;

public class Shares extends Entity implements Serializable, Remote, SharesTO {

    private static final long serialVersionUID = 7090847607189663018L;

    private String ssn;
    private String symbol;
    private Integer quantity;

    public Shares() {
        super();
    }

    public Shares(final Long id) {
        super(id);
    }

    public Shares(Long id, String ssn, String symbol, Integer quantity) {
        super(id);
        this.ssn = ssn;
        this.symbol = symbol;
        this.quantity = quantity;
    }

    @Override
    public String getSsn() {
        return ssn;
    }

    @Override
    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    @Override
    public String getSymbol() {
        return symbol;
    }

    @Override
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public Integer getQuantity() {
        return quantity;
    }

    @Override
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Shares shares = (Shares) o;

        if (ssn != null ? !ssn.equals(shares.ssn) : shares.ssn != null) return false;
        if (symbol != null ? !symbol.equals(shares.symbol) : shares.symbol != null) return false;
        return quantity != null ? quantity.equals(shares.quantity) : shares.quantity == null;
    }

    @Override
    public String toString() {
        return "Shares(super=" + super.toString() + ", ssn=" + this.getSsn() + ", symbol=" + this
                .getSymbol() + ", quantity=" + this.getQuantity() + ")";
    }

}
