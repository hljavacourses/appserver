package model.market;

import model.TransferObject;

public interface SharesTO extends TransferObject {

    String getSsn();

    void setSsn(String ssn);

    String getSymbol();

    void setSymbol(String symbol);

    Integer getQuantity();

    void setQuantity(Integer quantity);

}
