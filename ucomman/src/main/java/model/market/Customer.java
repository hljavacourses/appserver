package model.market;

import model.Entity;

import java.io.Serializable;
import java.rmi.Remote;

public class Customer extends Entity implements Serializable, Remote, CustomerTO {

    private static final long serialVersionUID = -4291243300097090600L;

    private String ssn;
    private String name;
    private String address;

    public Customer() {
        super();
    }

    public Customer(final Long id) {
        super(id);
    }

    public Customer(Long id, String ssn, String name, String address) {
        super(id);
        this.ssn = ssn;
        this.name = name;
        this.address = address;
    }

    @Override
    public String getSsn() {
        return ssn;
    }

    @Override
    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Customer customer = (Customer) o;

        if (ssn != null ? !ssn.equals(customer.ssn) : customer.ssn != null) return false;
        if (name != null ? !name.equals(customer.name) : customer.name != null) return false;
        return address != null ? address.equals(customer.address) : customer.address == null;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "Customer(super=" + super.toString() + ", ssn=" + this.getSsn() + ", name=" + this
                .getName() + ", address=" + this.getAddress() + ")";
    }

}
