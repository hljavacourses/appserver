package model.market;

import model.TransferObject;

public interface StockTO extends TransferObject {

    String getSymbol();

    void setSymbol(String symbol);

    Double getPrice();

    void setPrice(Double price);

}
