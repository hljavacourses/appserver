package model;

import java.io.Serializable;
import java.rmi.Remote;

public class User extends Entity implements Serializable, Remote, UserTO {

  private String name;
  private String passwd;

  public User() {
    super();
  }

  public User(final Long id) {
    super(id);
  }

  public User(Long id, String name, String passwd) {
    super(id);
    this.name = name;
    this.passwd = passwd;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getPasswd() {
    return passwd;
  }

  public void setPasswd(final String passwd) {
    this.passwd = passwd;
  }

  public boolean equals(final Object o) {
    if (o == this) {
      return true;
    }
    if (!(o instanceof User)) {
      return false;
    }
    final User other = (User) o;
    if (!other.canEqual(this)) {
      return false;
    }
    return super.equals(o);
  }

  protected boolean canEqual(final Object other) {
    return other instanceof User;
  }

  public int hashCode() {
    int result = super.hashCode();
    return result;
  }

  public String toString() {
    return "User(super=" + super.toString() + ", name=" + this.getName() + ", passwd=" + this
        .getPasswd() + ")";
  }
}
