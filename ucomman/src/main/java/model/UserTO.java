package model;

public interface UserTO extends TransferObject {

  Long getId();

  void setId(final Long id);

  String getName();

  void setName(final String name);

  String getPasswd();

  void setPasswd(final String passwd);
}
