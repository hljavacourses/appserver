package jndi;

import static javax.naming.Context.INITIAL_CONTEXT_FACTORY;
import static javax.naming.Context.PROVIDER_URL;

import java.util.Hashtable;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import rmi.RmiRegistry;
import rmi.custom.WithStubDescriptor;

public abstract class ServerRegisterContext implements RmiRegistry {

  private InitialContext ctx;

  public ServerRegisterContext(String host, int port) {
    System.setProperty(INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");

    Hashtable<String, String> jndiProperties = new Hashtable<>();
    jndiProperties.put(INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
    jndiProperties.put(PROVIDER_URL, host + ":" + port);
    try {
      this.ctx = new InitialContext(jndiProperties);
    } catch (NamingException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void bind(String name, WithStubDescriptor stub) {

  }

  @Override
  public void rebind(String name, WithStubDescriptor stub) {

  }
//
//  @Override
//  public <T extends WithStubDescriptor> T lookup(String name, Class<T> type) {
//    return null;
//  }
//
//  @Override
//  public void bind(String name, StubDescriptor stub) {
//    try {
//      ctx.bind(name, stub);
//    } catch (NamingException e) {
//      throw new RuntimeException(e);
//    }
//  }
//
//  @Override
//  public void rebind(String name, StubDescriptor stub) {
//    try {
//      ctx.rebind(name, stub);
//    } catch (NamingException e) {
//      throw new RuntimeException(e);
//    }
//  }
//
//  @Override
//  public StubDescriptor lookup(String name) {
//    StubDescriptor object;
//    try {
//      // FIXME:
//      object = (StubDescriptor)ctx.lookup(name);
//    } catch (NamingException e) {
//      throw new RuntimeException(e);
//    }
//    return object;
//  }
//
//  @Override
//  public <T> T lookup(String name, Class<T> type) {
//    throw new UnsupportedOperationException("No implemented");
//  }

  @Override
  public void start() {
    throw new UnsupportedOperationException("No implemented");
  }

}
