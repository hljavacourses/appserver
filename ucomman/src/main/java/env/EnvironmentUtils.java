package env;

/**
 * Набор методов для получения знаничний переменных среды
 *
 * https://en.wikipedia.org/wiki/Environment_variable
 */
public class EnvironmentUtils {

  public static int envInt(String name, Integer defaultValue) {
    final String value = System.getenv(name);
    return value == null ? defaultValue : Integer.parseInt(value);
  }

  public static String envString(String name, String defaultValue) {
    final String value = System.getenv(name);
    return value == null ? defaultValue : value;
  }
}
