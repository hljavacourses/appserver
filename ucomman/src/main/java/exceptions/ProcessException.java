package exceptions;

public class ProcessException extends RuntimeException {

  public ProcessException(Throwable e) {
    super(e);
  }

  public ProcessException(String s) {
    super(s);
  }
}
