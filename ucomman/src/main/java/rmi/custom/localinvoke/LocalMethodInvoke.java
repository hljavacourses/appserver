package rmi.custom.localinvoke;

import rmi.custom.Request;
import rmi.custom.Responce;

@FunctionalInterface
public interface LocalMethodInvoke {

  Responce invoke(Request req);
}
