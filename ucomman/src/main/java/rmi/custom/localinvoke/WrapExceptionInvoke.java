package rmi.custom.localinvoke;

import static java.util.Objects.requireNonNull;

import rmi.custom.Request;
import rmi.custom.Responce;

public class WrapExceptionInvoke implements LocalMethodInvoke {

  private LocalMethodInvoke delegate;

  public WrapExceptionInvoke(LocalMethodInvoke delegate) {
    this.delegate = requireNonNull(delegate);
  }

  @Override
  public Responce invoke(Request req) {
    try {
      return delegate.invoke(req);
    } catch (Exception e) {
      final Throwable cause = e.getCause();
      return Responce.Error(cause);
    }
  }
}
