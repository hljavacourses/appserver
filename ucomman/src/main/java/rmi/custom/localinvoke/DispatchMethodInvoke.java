package rmi.custom.localinvoke;

import java.util.Map;
import rmi.custom.Request;
import rmi.custom.Responce;

public class DispatchMethodInvoke implements LocalMethodInvoke {

  private final Map<Long, LocalMethodInvoke> delegates;

  public DispatchMethodInvoke(
      Map<Long, LocalMethodInvoke> delegates) {
    this.delegates = delegates;
  }

  @Override
  public Responce invoke(Request req) {
    return delegates.get(req.oid).invoke(req);
  }
}
