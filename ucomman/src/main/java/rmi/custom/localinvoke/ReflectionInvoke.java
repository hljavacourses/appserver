package rmi.custom.localinvoke;

import static java.util.Objects.requireNonNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import rmi.custom.Request;
import rmi.custom.Responce;

public class ReflectionInvoke implements LocalMethodInvoke {

  private Object delegate;

  public ReflectionInvoke(Object delegate) {
    this.delegate = requireNonNull(delegate);
  }

  @Override
  public Responce invoke(Request req) {
    final Method method;
    try {
      method = delegate.getClass().getMethod(req.name, req.signature);
      final Object result = method.invoke(delegate, req.arguments);
      return Responce.Ok(result);
    } catch (NoSuchMethodException | IllegalAccessException e) {
      throw new RuntimeException(e);
    } catch (InvocationTargetException e) {
      throw new RuntimeException(e.getCause());
    }
  }
}
