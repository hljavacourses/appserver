package rmi.custom;

import java.io.Serializable;

public class RegistryResponse implements Serializable {

  ResultType resultType;
  Exception failure;
  Serializable result;

  private RegistryResponse(ResultType success, Serializable result, Exception failure) {
    resultType = success;
    this.failure = failure;
    this.result = result;
  }

  static RegistryResponse OK(Serializable result) {
    return new RegistryResponse(ResultType.SUCCESS, result, null);
  }

  static RegistryResponse Failure(Exception failure) {
    return new RegistryResponse(ResultType.FAIL, null, failure);
  }

  public enum ResultType implements Serializable {
    SUCCESS, FAIL
  }
}
