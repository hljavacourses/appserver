package rmi.custom;

import java.io.Serializable;

public class Responce implements Serializable {

  public final boolean successful;
  public final Object result;
  public final Throwable failure;

  private Responce(boolean successful, Object result, Throwable failure) {
    this.successful = successful;
    this.result = result;
    this.failure = failure;
  }

  public static Responce Ok(Object result) {
    return new Responce(true, result, null);
  }

  public static Responce Error(Throwable failure) {
    return new Responce(false, null, failure);
  }
}
