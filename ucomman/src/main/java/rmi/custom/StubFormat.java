package rmi.custom;

/**
 * Объединяет методы работы со stub-ом конкретной реализации.
 */
public interface StubFormat {

  /**
   * Создает stub для объекта в текущем процессе.
   *
   * Например созданный stub может делегировать вызовы объекту напрямую.
   *
   * @param original серверный объект
   * @param metainf информация необходимая для удаленного вызова
   * @return stub
   */
  <T extends WithStubDescriptor> T create(Object original, StubDescriptor metainf);

  /**
   * Преобразует stub созданный на сервервере представление для передачи по сети.
   *
   * @param stub stub
   * @return представление stub для передачи по сети
   */
  StubDescriptor serialize(WithStubDescriptor stub);

  /**
   * Востанавливает stub из сериализованного представления
   *
   * @param serializedStub сериализованное представление
   * @return stub stub способный делать удаленные вызовы
   */
  <T extends  WithStubDescriptor> T recover(StubDescriptor serializedStub);
}
