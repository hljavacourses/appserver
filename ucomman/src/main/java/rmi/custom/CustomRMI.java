package rmi.custom;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import rmi.Skeleton;
import rmi.custom.localinvoke.DispatchMethodInvoke;
import rmi.custom.localinvoke.LocalMethodInvoke;
import rmi.custom.localinvoke.ReflectionInvoke;
import rmi.custom.localinvoke.WrapExceptionInvoke;

public class CustomRMI implements Skeleton {

  private final int port;
  private final String host;
  private final ExecutorService executor;
  private final StubFormat stubFormat;

  private Map<Long, Object> serverObjects = new ConcurrentHashMap<>();
  private AtomicLong idGenerator = new AtomicLong(0);

  public CustomRMI(int port) {
    this(port, Executors.newFixedThreadPool(5), new StubDescriptionFormat());
  }

  public CustomRMI(int port, String host, ExecutorService executor, StubFormat stubFormat) {
    this.port = port;
    this.host = host;
    this.executor = executor;
    this.stubFormat = stubFormat;
  }

  public CustomRMI(int port, ExecutorService executor, StubFormat stubFormat) {
    this(port, "localhost", executor, stubFormat);
  }

  @Override
  public <T, R extends WithStubDescriptor> R exportObject(T serverObject) {
    final long id = idGenerator.incrementAndGet();
    serverObjects.put(id, serverObject);
    return stubFormat.create(serverObject, new StubDescriptor(host, port, id, null //
    ));
  }

  public void start() {
    try {
      try (ServerSocket serverSocket = new ServerSocket(port)) {
        System.out.println("Server proxy listens port " + port);
        final ConcurrentMap<Long, LocalMethodInvoke> localInvokes = serverObjects.entrySet()
            .stream()
            .collect(Collectors.toConcurrentMap(Entry::getKey,
                x -> new WrapExceptionInvoke(new ReflectionInvoke(x.getValue()))));
        final DispatchMethodInvoke localInvoke = new DispatchMethodInvoke(localInvokes);
        while (true) {
          final Socket connection = serverSocket.accept();
          final SocketLocalInvokeTask task = new SocketLocalInvokeTask(connection, localInvoke);
          executor.submit(task);
        }
      } finally {
        System.out.println("Server proxy stopped");
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
