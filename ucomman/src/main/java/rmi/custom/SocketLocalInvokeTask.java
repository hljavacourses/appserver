package rmi.custom;

import static java.util.Objects.requireNonNull;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import rmi.custom.localinvoke.LocalMethodInvoke;

public class SocketLocalInvokeTask implements LocalInvokeTask {

  private final Socket connection;

  private final LocalMethodInvoke invoke;

  public SocketLocalInvokeTask(Socket connection, LocalMethodInvoke invoke) {
    this.connection = requireNonNull(connection);
    this.invoke = requireNonNull(invoke);
  }

  @Override
  public Object call() throws Exception {
    try {
      System.out.println("Server proxy accepted connection");
      final ObjectOutputStream oos = new ObjectOutputStream(
          connection.getOutputStream());
      final ObjectInputStream ois = new ObjectInputStream(
          connection.getInputStream());
      try {
        final Request request = (Request) ois.readObject();
        final Responce result = invoke.invoke(request);
        oos.writeObject(result);
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
        oos.writeObject(Responce.Error(e));
      }
    } finally {
      System.out.println("Connection closed");
      connection.close();
    }
    return null;
  }
}
