package rmi.custom;

import java.io.Serializable;

public class Request implements Serializable {

  public final long oid;
  public final String name;
  public final Class[] signature;
  public final Object[] arguments;

  public Request(long oid, String name, Class[] signature, Object[] arguments) {
    this.oid = oid;
    this.name = name;
    this.signature = signature;
    this.arguments = arguments;
  }
}
