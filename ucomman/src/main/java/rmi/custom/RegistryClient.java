package rmi.custom;

import static rmi.custom.RegistryRequest.Method.BIND;
import static rmi.custom.RegistryRequest.Method.LOOKUP;
import static rmi.custom.RegistryRequest.Method.REBIND;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import rmi.RmiRegistry;
import rmi.custom.RegistryResponse.ResultType;

public class RegistryClient implements RmiRegistry {

  private final String host;
  private final int port;
  private final StubFormat stubFormat;

  public RegistryClient(String host, int port, StubFormat stubFormat) {
    this.host = host;
    this.port = port;
    this.stubFormat = stubFormat;
  }

  @Override
  public void bind(String name, WithStubDescriptor stub) {
    final RegistryResponse res = doRequest(
        new RegistryRequest(BIND, name, stubFormat.serialize(stub)));
    handleResult(res);
  }

  private RegistryResponse doRequest(RegistryRequest req) {
    try (Socket connection = new Socket(host, port)) {
      final ObjectOutputStream oos = new ObjectOutputStream(connection.getOutputStream());
      oos.writeObject(req);
      oos.flush();
      final ObjectInputStream ois = new ObjectInputStream(connection.getInputStream());
      return (RegistryResponse) ois.readObject();
    } catch (IOException | ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }

  private Serializable handleResult(RegistryResponse res) {
    if (res.resultType == ResultType.SUCCESS) {
      return res.result;
    } else {
      throw new RuntimeException(res.failure);
    }
  }

  @Override
  public void rebind(String name, WithStubDescriptor stub) {
    final RegistryResponse res = doRequest(new RegistryRequest(REBIND, name,
        stubFormat.serialize(stub)));
    handleResult(res);
  }

  @Override
  public WithStubDescriptor lookup(String name) {
    final RegistryResponse res = doRequest(new RegistryRequest(LOOKUP, name, null));
    final StubDescriptor stubDescriptor = (StubDescriptor) handleResult(res);
    return stubFormat.recover(stubDescriptor);
  }

  @Override
  public <T> T lookup(String name, Class<T> type) {
    final RegistryResponse res = doRequest(new RegistryRequest(LOOKUP, name, null));
    // TODO: обработать случай, когда сервис не найден в реестре
    final StubDescriptor stubDescriptor = (StubDescriptor) handleResult(res);
    final StubDescriptor descriptorForClientType = new StubDescriptor(stubDescriptor.address,
        stubDescriptor.port, stubDescriptor.oid, type);
    return stubFormat.recover(descriptorForClientType);
  }
//  @Override
//  public WithStubDescriptor lookup(String name, Class<T> type) {
//    final RegistryResponse res = doRequest(new RegistryRequest(LOOKUP, name, null));
//    final StubDescriptor stubDescriptor = (StubDescriptor) handleResult(res);
//    final StubDescriptor descriptorForClientType = new StubDescriptor(stubDescriptor.address,
//        stubDescriptor.port, stubDescriptor.oid, type);
//    return (T) stubFormat.recover(descriptorForClientType);
//  }

  @Override
  public void start() {

  }

}
