package rmi.custom;

import java.io.Serializable;

public class RegistryRequest implements Serializable {

  Method method;
  String name;
  StubDescriptor object;

  public RegistryRequest(Method bind, String name, StubDescriptor object) {
    method = bind;
    this.name = name;
    this.object = object;
  }

  public enum Method implements Serializable {
    BIND, REBIND, LOOKUP
  }
}
