package rmi.custom;

import java.io.Serializable;

public class StubDescriptor implements Serializable {

  public final String address;
  public final int port;
  public final Long oid;
  public final Class stubClazz;

  public StubDescriptor(String address, int port, Long oid, Class stubClazz) {
    this.address = address;
    this.port = port;
    this.oid = oid;
    this.stubClazz = stubClazz;
  }
}
