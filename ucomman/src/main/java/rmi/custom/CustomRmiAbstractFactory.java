package rmi.custom;

import static env.EnvironmentUtils.envInt;
import static env.EnvironmentUtils.envString;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import rmi.RmiAbstractFactory;
import rmi.RmiRegistry;
import rmi.Skeleton;

public class CustomRmiAbstractFactory implements RmiAbstractFactory {

  private final int skeletonPort;
  private final String skeletonHost;
  private final int registryPort;
  private final String registryHost;

  public CustomRmiAbstractFactory() {
    this(envInt("skeleton.port", 5010),
        envString("skeleton.host", "localhost"),
        envInt("registry.port", 5011),
        envString("registry.host", "localhost"));
  }

  public CustomRmiAbstractFactory(int skeletonPort, String skeletonHost, int registryPort,
      String registryHost) {
    this.skeletonPort = skeletonPort;
    this.skeletonHost = skeletonHost;
    this.registryPort = registryPort;
    this.registryHost = registryHost;
  }

  private CustomRmiAbstractFactory(int skeletonPort, int registryPort, String registryHost) {
    this(skeletonPort, "localhost", registryPort, registryHost);
  }

  public CustomRmiAbstractFactory(int skeletonPort) {
    this(skeletonPort, 8081, "localhost");
  }

  public CustomRmiAbstractFactory(int skeletonPort, int registryPort) {
    this(skeletonPort, registryPort, "localhost");
  }

  @Override
  public Skeleton createSkeleton() {
    return new CustomRMI(skeletonPort, skeletonHost, executor(), stubFormat());
  }

  private ExecutorService executor() {
    return Executors.newFixedThreadPool(5);
  }

  private StubDescriptionFormat stubFormat() {
    return new StubDescriptionFormat();
  }

  @Override
  public Registry createRegistry() {
    return new LocalRegistry(registryPort, stubFormat());
  }

  @Override
  public RmiRegistry getRegistry() {
    return new RegistryClient(registryHost, registryPort, stubFormat());
  }

}
