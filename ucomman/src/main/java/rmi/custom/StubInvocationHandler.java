package rmi.custom;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.Socket;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class StubInvocationHandler implements InvocationHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(StubInvocationHandler.class);

  private final StubDescriptor descriptor;

  public StubInvocationHandler(StubDescriptor descriptor) {
    this.descriptor = descriptor;
  }

  @Override
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    try {
      LOGGER.debug("RMI call to address: {}, with port: {}", descriptor.address, descriptor.port);
      try (final Socket socket = new Socket(descriptor.address, descriptor.port)) {
        final ObjectOutputStream oos = new ObjectOutputStream(
            socket.getOutputStream());
        final ObjectInputStream ois = new ObjectInputStream(
            socket.getInputStream());
        final Class[] signature = Arrays.stream(method.getParameters())
            .map(Parameter::getType)
            .toArray(Class[]::new);
        final Request request = new Request(descriptor.oid, method.getName(),
            signature,
            args);
        oos.writeObject(request);
        oos.flush();
        final Responce responce = (Responce) ois.readObject();
        if (responce.successful) {
          return responce.result;
        } else {
          throw responce.failure;
        }
      }
    } catch (IOException | ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }
}
