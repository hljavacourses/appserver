package rmi.custom;

@FunctionalInterface
public interface WithStubDescriptor {

  StubDescriptor stubDescriptor();
}
