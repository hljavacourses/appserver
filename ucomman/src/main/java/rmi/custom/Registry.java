package rmi.custom;

public interface Registry {

  void bind(String name, StubDescriptor stub);

  void rebind(String name, StubDescriptor stub);

  StubDescriptor lookup(String name);

  void start();
}
