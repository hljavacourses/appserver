package rmi.custom;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.implementation.FixedValue;
import net.bytebuddy.implementation.InvocationHandlerAdapter;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatchers;

public class StubDescriptionFormat implements StubFormat {

  @Override
  public <T extends WithStubDescriptor> T create(Object original, StubDescriptor metainf) {
    try {
      return (T) new ByteBuddy()
          .subclass(original.getClass())
          .method(ElementMatchers.anyOf(original.getClass().getMethods()))
          .intercept(MethodDelegation.to(original))
          .implement(WithStubDescriptor.class)
          .method(ElementMatchers.anyOf(WithStubDescriptor.class.getMethods()))
          .intercept(FixedValue.value(metainf))
          .make().load(this.getClass().getClassLoader()).getLoaded().newInstance();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

  }

  @Override
  public StubDescriptor serialize(WithStubDescriptor stub) {
    return stub.stubDescriptor();
  }

  @Override
  public WithStubDescriptor recover(StubDescriptor serializedStub) {
    try {
      final StubDescriptor descriptor = serializedStub;
      return (WithStubDescriptor) new ByteBuddy().subclass(descriptor.stubClazz)
          .method(ElementMatchers.anyOf(descriptor.stubClazz.getMethods()))
          .intercept(InvocationHandlerAdapter.of(new StubInvocationHandler(descriptor)))
          .implement(WithStubDescriptor.class)
          .method(ElementMatchers.anyOf(WithStubDescriptor.class.getMethods()))
          .intercept(FixedValue.value(serializedStub))
          .make()
          .load(this.getClass().getClassLoader()).getLoaded().newInstance();
    } catch (InstantiationException | IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }
}
