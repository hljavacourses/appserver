package rmi.custom;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.registry.LocateRegistry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LocalRegistry implements Registry {

  private static final Logger LOGGER = LoggerFactory.getLogger(LocateRegistry.class);

  private final int port;
  private final ConcurrentMap<String, StubDescriptor> objects = new ConcurrentHashMap<>();
  private final StubFormat stubFormat;

  public LocalRegistry(int port, StubFormat stubFormat) {
    this.port = port;
    this.stubFormat = stubFormat;
  }

  @Override
  public void bind(String name, StubDescriptor stub) {
    if (objects.containsKey(name)) {
      throw new IllegalArgumentException("Object with name [" + name + "] already bind");
    }
    objects.put(name, stub);
  }

  @Override
  public void rebind(String name, StubDescriptor stub) {
    objects.put(name, stub);
  }

  @Override
  public StubDescriptor lookup(String name) {
    return objects.get(name);
  }

  @Override
  public void start() {
    try {
      try (ServerSocket serverSocket = new ServerSocket(port)) {
        while (true) {
          LOGGER.info("Listen port {}", port);
          try (Socket connection = serverSocket.accept()) {
            try {
              final ObjectInputStream ois = new ObjectInputStream(connection.getInputStream());
              final RegistryRequest name = (RegistryRequest) ois.readObject();
              final RegistryResponse res = handleRequest(name);
              final ObjectOutputStream oos = new ObjectOutputStream(connection.getOutputStream());
              oos.writeObject(res);
            } catch (ClassNotFoundException e) {
              e.printStackTrace();
            }
          }
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private RegistryResponse handleRequest(RegistryRequest name) {
    try {
      switch (name.method) {
        case BIND:
          bind(name.name, name.object);
          return RegistryResponse.OK(null);
        case REBIND:
          rebind(name.name, name.object);
          return RegistryResponse.OK(null);
        case LOOKUP:
          final StubDescriptor result = lookup(name.name);
          return RegistryResponse.OK(result);
        default:
          throw new IllegalArgumentException("Unhandled enum value [" + name.method + "]");
      }
    } catch (Exception e) {
      e.printStackTrace();
      return RegistryResponse.Failure(e);
    }
  }
}
