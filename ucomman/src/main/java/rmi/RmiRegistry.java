package rmi;

import rmi.custom.WithStubDescriptor;

public interface RmiRegistry {

  void bind(String name, WithStubDescriptor stub);

  void rebind(String name, WithStubDescriptor stub);

  Object lookup(String name);

  <T> T lookup(String name, Class<T> type);

  void start();
}
