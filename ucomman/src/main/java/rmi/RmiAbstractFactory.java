package rmi;

import rmi.custom.Registry;

public interface RmiAbstractFactory {

  Skeleton createSkeleton();

  Registry createRegistry();

  RmiRegistry getRegistry();
}
