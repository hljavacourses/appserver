package command;

import java.io.Serializable;
import java.rmi.Remote;
import model.TransferObject;

public interface ServerCommandManager extends Remote, Serializable {

  <T, D extends TransferObject> D execute(final Class<T> clazz, D obj)
      throws Exception;

}
