package domain.support.jdbc;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

import exceptions.ProcessException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Entity;
import reflect.ClassUtils;
import sqlbuilder.EntityDescriptor;

public class JdbcDaoSupport {

  private final ConnectionPool pool;

  public JdbcDaoSupport(ConnectionPool pool) {
    this.pool = pool;
  }

  private Connection getConnection() throws SQLException {
    return pool.getConnection();
  }

  public Long getId(String sql) {
    Long id = null;

    try (Connection con = getConnection(); Statement statement = con
        .createStatement(); ResultSet rs = statement.executeQuery(sql)) {
      if (rs.next()) {
        id = rs.getLong(1);
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }

    return id;
  }

  public <T> T selectOne(String sql, RowMapper<T> rowMapper, Long param) {
    T obj = null;

    try (Connection con = getConnection(); PreparedStatement statement = con
        .prepareStatement(sql)) {

      statement.setObject(1, param);

      try (ResultSet rs = statement.executeQuery()) {
        if (rs.next()) {
          obj = rowMapper.rowMap(rs);
        }
      }
    } catch (SQLException | IllegalAccessException e) {
      throw new RuntimeException(e);
    }

    return obj;
  }

  public <T> List<T> selectList(String sql, RowMapper<T> rowMap, T param) {
    System.out.println(sql + "\n" + param);
    List<T> objs = new ArrayList<>();

    try (Connection con = getConnection(); PreparedStatement statement = con
        .prepareStatement(sql)) {
      if (param != null) {
        List<Field> fields = ClassUtils.getFields(param.getClass());
        for (int i = 0; i < fields.size(); i++) {
          statement.setObject(i + 1, fields.get(i));
        }
      }
      try (ResultSet rs = statement.executeQuery()) {
        while (rs.next()) {
          objs.add(rowMap.rowMap(rs));
        }
      }
    } catch (SQLException | IllegalAccessException e) {
      throw new RuntimeException(e);
    }

    return objs;
  }

  public <T extends Entity> void update(String sql, T param) {
    try (Connection con = getConnection(); PreparedStatement statement = con
        .prepareStatement(sql)) {
      final EntityDescriptor descr = new EntityDescriptor(param.getClass());
      final List<Object> values = descr.values(param);
      int i;
      for (i = 0; i < values.size(); i++) {
        statement.setObject(i + 1, values.get(i));
      }
      statement.setObject(i + 1, descr.id(param));
      int result = statement.executeUpdate();
      if (result != 1) {
        throw new ProcessException("Expected to update 1 row, but updated " + result + " rows");
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public <T extends Entity> void create(String sql, T param) {
    System.out.println(sql);
    System.out.println(param);
    try (Connection con = getConnection(); PreparedStatement statement = con
        .prepareStatement(sql, RETURN_GENERATED_KEYS)) {
      EntityDescriptor descr = new EntityDescriptor(param.getClass());
      List<Object> valuesWithoutId = descr.valuesWithoutId(param);
      for (int i = 0; i < valuesWithoutId.size(); i++) {
        Object o = valuesWithoutId.get(i);
        statement.setObject(i + 1, o);
      }
      int result = statement.executeUpdate();
      if (result != 1) {
        throw new ProcessException("Expected to update 1 row, but updated " + result + " rows");
      }
      ResultSet generatedKeys = statement.getGeneratedKeys();
      while (generatedKeys.next()) {
        Long id = generatedKeys.getLong(descr.idName());
        param.setId(id);
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public void delete(String sql, Long param) {
    try (Connection con = getConnection(); PreparedStatement statement = con
        .prepareStatement(sql)) {
      statement.setLong(1, param);
      statement.execute();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }
}
