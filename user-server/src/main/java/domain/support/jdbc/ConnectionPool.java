package domain.support.jdbc;


import static java.util.Collections.synchronizedList;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.sql.DataSource;

public class ConnectionPool {

  private int min;
  private int max;
  private DataSource ds;
  private List<PooledConnection> free = synchronizedList(new ArrayList<>());
  private List<PooledConnection> used = synchronizedList(new ArrayList<>());

  public ConnectionPool(int min, int max, DataSource ds) {
    this.min = min;
    this.max = max;
    this.ds = ds;
    init();
  }

  private void init() {
    for (int i = 0; i < this.min; i++) {
      free.add(createPooledConnection());
    }
  }

  public PooledConnection createPooledConnection() {
    try {
      Connection con = ds.getConnection();
      PooledConnection pcon = new PooledConnection(this, con);
      return pcon;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  protected void freePooledConnection(PooledConnection pcon) {
    used.remove(pcon);
    free.add(pcon);
  }

  public Connection getConnection() throws SQLException {
    PooledConnection pcon = null;
    if (!free.isEmpty()) {
      pcon = free.remove(free.size() - 1);
    } else if (used.size() < max) {
      pcon = createPooledConnection();
    } else {
      throw new RuntimeException("Unable to create a new connection");
    }
    used.add(pcon);
    return pcon;
  }

  @Override
  protected void finalize() throws Throwable {
    for (PooledConnection pcon : free) {
      try {
        pcon.getOriginal().close();
      } catch (Exception e) {
      }
    }
    for (PooledConnection pcon : used) {
      try {
        pcon.getOriginal().close();
      } catch (Exception e) {
      }
    }
  }
}
