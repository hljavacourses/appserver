package dao.market;

import domain.support.jdbc.JdbcDaoSupport;
import domain.support.jdbc.RowMapper;
import domain.support.jdbc.RowMapperImpl;
import model.market.Customer;
import sqlbuilder.SqlBuilder;

import java.util.List;

public class CustomerJdbcDao implements CustomerDao {

    private RowMapper<Customer> rowMap = new RowMapperImpl<>(Customer.class);
    private SqlBuilder builder = new SqlBuilder();
    private JdbcDaoSupport jdbcDaoSupport;

    public CustomerJdbcDao(JdbcDaoSupport jdbcDaoSupport) {
        this.jdbcDaoSupport = jdbcDaoSupport;
    }

    @Override
    public List<Customer> findAll() {
        return jdbcDaoSupport.selectList(builder.getSelectSQL(Customer.class), rowMap, null);
    }

    @Override
    public void create(final Customer obj) {
        jdbcDaoSupport.create(builder.getInsertSQL(obj), obj);
    }

    @Override
    public Customer read(final Long id) {
        return jdbcDaoSupport.selectOne("", rowMap, id);
    }

    @Override
    public void update(final Customer obj) {
        jdbcDaoSupport.update(builder.getUpdateSQL(obj.getClass()), obj);
    }

    @Override
    public void delete(final Long id) {
        jdbcDaoSupport.delete(builder.getDeleteSQL(Customer.class), id);
    }

}
