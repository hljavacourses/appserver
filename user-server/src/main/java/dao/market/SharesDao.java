package dao.market;

import dao.CrudDao;
import model.market.Shares;

import java.util.List;

public interface SharesDao extends CrudDao<Long, Shares> {
    List<Shares> findAll();
}
