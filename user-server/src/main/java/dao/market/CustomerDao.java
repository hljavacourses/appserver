package dao.market;

import dao.CrudDao;
import model.market.Customer;

import java.util.List;

public interface CustomerDao extends CrudDao<Long, Customer> {
    List<Customer> findAll();
}
