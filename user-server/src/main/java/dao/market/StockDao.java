package dao.market;

import dao.CrudDao;
import model.market.Stock;

import java.util.List;

public interface StockDao extends CrudDao<Long, Stock> {
    List<Stock> findAll();
}
