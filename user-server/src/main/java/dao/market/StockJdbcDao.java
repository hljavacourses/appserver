package dao.market;

import domain.support.jdbc.JdbcDaoSupport;
import domain.support.jdbc.RowMapper;
import domain.support.jdbc.RowMapperImpl;
import model.market.Stock;
import sqlbuilder.SqlBuilder;

import java.util.List;

public class StockJdbcDao implements StockDao {

    private RowMapper<Stock> rowMap = new RowMapperImpl<>(Stock.class);
    private SqlBuilder builder = new SqlBuilder();
    private JdbcDaoSupport jdbcDaoSupport;

    public StockJdbcDao(JdbcDaoSupport jdbcDaoSupport) {
        this.jdbcDaoSupport = jdbcDaoSupport;
    }

    @Override
    public List<Stock> findAll() {
        return jdbcDaoSupport.selectList(builder.getSelectSQL(Stock.class), rowMap, null);
    }

    @Override
    public void create(final Stock obj) {
        jdbcDaoSupport.create(builder.getInsertSQL(obj), obj);
    }

    @Override
    public Stock read(final Long id) {
        return jdbcDaoSupport.selectOne("", rowMap, id);
    }

    @Override
    public void update(final Stock obj) {
        jdbcDaoSupport.update(builder.getUpdateSQL(obj.getClass()), obj);
    }

    @Override
    public void delete(final Long id) {
        jdbcDaoSupport.delete(builder.getDeleteSQL(Stock.class), id);
    }

}
