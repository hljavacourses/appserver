package dao.market;

import domain.support.jdbc.JdbcDaoSupport;
import domain.support.jdbc.RowMapper;
import domain.support.jdbc.RowMapperImpl;
import model.market.Shares;
import sqlbuilder.SqlBuilder;

import java.util.List;

public class SharesJdbcDao implements SharesDao {

    private RowMapper<Shares> rowMap = new RowMapperImpl<>(Shares.class);
    private SqlBuilder builder = new SqlBuilder();
    private JdbcDaoSupport jdbcDaoSupport;

    public SharesJdbcDao(JdbcDaoSupport jdbcDaoSupport) {
        this.jdbcDaoSupport = jdbcDaoSupport;
    }

    @Override
    public List<Shares> findAll() {
        return jdbcDaoSupport.selectList(builder.getSelectSQL(Shares.class), rowMap, null);
    }

    @Override
    public void create(final Shares obj) {
        jdbcDaoSupport.create(builder.getInsertSQL(obj), obj);
    }

    @Override
    public Shares read(final Long id) {
        return jdbcDaoSupport.selectOne("", rowMap, id);
    }

    @Override
    public void update(final Shares obj) {
        jdbcDaoSupport.update(builder.getUpdateSQL(obj.getClass()), obj);
    }

    @Override
    public void delete(final Long id) {
        jdbcDaoSupport.delete(builder.getDeleteSQL(Shares.class), id);
    }

}
