package dao;

import java.util.List;

public interface SearchDao<T> extends Dao<T>  {
    List<T> findAll();
}
