package dao.user;

import domain.support.jdbc.JdbcDaoSupport;
import domain.support.jdbc.RowMapper;
import domain.support.jdbc.RowMapperImpl;
import java.util.List;
import model.User;
import sqlbuilder.SqlBuilder;

public class UserJdbcDao implements UserDao {

  private RowMapper<User> rowMap = new RowMapperImpl<>(User.class);
  private SqlBuilder builder = new SqlBuilder();
  private JdbcDaoSupport jdbcDaoSupport;

  public UserJdbcDao(JdbcDaoSupport jdbcDaoSupport) {
    this.jdbcDaoSupport = jdbcDaoSupport;
  }

  @Override
  public List<User> findAll() {
    return jdbcDaoSupport.selectList(builder.getSelectSQL(User.class), rowMap, null);
  }

  @Override
  public void create(final User obj) {
    jdbcDaoSupport.create(builder.getInsertSQL(obj), obj);
  }

  @Override
  public User read(final Long id) {
    return jdbcDaoSupport.selectOne("", rowMap, id);
  }

  @Override
  public void update(final User obj) {
    jdbcDaoSupport.update(builder.getUpdateSQL(obj.getClass()), obj);
  }

  @Override
  public void delete(final Long id) {
    jdbcDaoSupport.delete(builder.getDeleteSQL(User.class), id);
  }
}
