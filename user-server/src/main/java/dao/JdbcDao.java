package dao;

import domain.support.jdbc.JdbcDaoSupport;
import domain.support.jdbc.RowMapper;
import domain.support.jdbc.RowMapperImpl;
import model.Entity;
import sqlbuilder.SqlBuilder;

import java.util.List;

public class JdbcDao<T extends Entity> implements CrudDao<Long, T>, SearchDao<T> {

    private Class<T> clazz;
    private JdbcDaoSupport jdbcDaoSupport;
    private RowMapper<T> rowMap;
    private SqlBuilder builder;

    public JdbcDao(JdbcDaoSupport jdbcDaoSupport, Class<T> clazz) {
        this.clazz = clazz;
        this.jdbcDaoSupport = jdbcDaoSupport;
        this.rowMap = new RowMapperImpl<>(clazz);
        this.builder = new SqlBuilder();
    }

    @Override
    public List<T> findAll() {
        return jdbcDaoSupport.selectList(builder.getSelectSQL(clazz), rowMap, null);
    }

    @Override
    public void create(final T obj) {
        jdbcDaoSupport.create(builder.getInsertSQL(obj), obj);
    }

    @Override
    public T read(final Long id) {
        return jdbcDaoSupport.selectOne("", rowMap, id);
    }

    @Override
    public void update(final T obj) {
        jdbcDaoSupport.update(builder.getUpdateSQL(obj.getClass()), obj);
    }

    @Override
    public void delete(final Long id) {
        jdbcDaoSupport.delete(builder.getDeleteSQL(clazz), id);
    }

}
