package command;

import exceptions.ProcessException;
import java.rmi.RemoteException;
import java.util.concurrent.ExecutionException;
import model.TransferObject;

/**
 * Is necessary to log exceptions via remote call
 */
public class ExceptionHandlerServerCommandManager implements ServerCommandManager {

  private ServerCommandManager origin;

  public ExceptionHandlerServerCommandManager() {
    // Custom RMI constructor
  }

  public ExceptionHandlerServerCommandManager(ServerCommandManager origin) {
    this.origin = origin;
  }

  @Override
  public <T, D extends TransferObject> D execute(Class<T> clazz, D obj)
      throws RemoteException, ExecutionException, InterruptedException {
    try {
      return origin.execute(clazz, obj);
    } catch (Throwable e) {
      e.printStackTrace();
      throw new ProcessException(e);
    }
  }

  public ServerCommandManager getOrigin() {
    return origin;
  }
}
