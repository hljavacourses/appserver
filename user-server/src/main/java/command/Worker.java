package command;

import static java.util.Objects.requireNonNull;

import java.util.concurrent.Callable;
import model.TransferObject;

public class Worker<T extends TransferObject> implements Callable<T> {

  private Command<T> command;
  private T transferObject;

  public Worker(final Command<T> command, final T transferObject) {
    this.command = requireNonNull(command,"command is null");
    this.transferObject = requireNonNull(transferObject, "TO is null");
  }

  @Override
  public T call() throws Exception {
    return command.execute(transferObject);
  }
}
