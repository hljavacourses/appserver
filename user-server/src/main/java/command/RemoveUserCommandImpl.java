package command;

import annotation.ServerCommand;
import dao.user.UserDao;
import java.rmi.RemoteException;
import model.UserTO;

@ServerCommand(name = "RemoveUserCommand")
public class RemoveUserCommandImpl implements RemoveUserCommand {

  private UserDao userDao;

  public RemoveUserCommandImpl() {
    // CustomRMI constructor
  }

  public RemoveUserCommandImpl(UserDao userDao) throws RemoteException {
    this.userDao = userDao;
  }

  @Override
  public UserTO execute(final UserTO userTO) throws RemoteException {
    userDao.delete(userTO.getId());
    return userTO;
  }
}
