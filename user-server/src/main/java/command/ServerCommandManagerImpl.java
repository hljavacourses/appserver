package command;

import annotation.ServerCommand;
import dao.user.UserDao;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import model.TransferObject;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rmi.RmiRegistry;
import rmi.Skeleton;
import rmi.custom.WithStubDescriptor;

public class ServerCommandManagerImpl implements ServerCommandManager {

  //  private Map<Class, Command> commands;
  private static final Logger logger = LoggerFactory.getLogger(ServerCommandManagerImpl.class);
  private final Skeleton skeleton;
  private final RmiRegistry registry;
  private final ExecutorService executorService;

  public ServerCommandManagerImpl(RmiRegistry registry, Skeleton skeleton,
      ExecutorService executorService) {
    this.registry = registry;
    this.skeleton = skeleton;
    this.executorService = executorService;
  }

  @Override
  public <T, D extends TransferObject> D execute(final Class<T> clazz, D obj)
      throws Exception {

    logger.info("Lookup & submit command {}", clazz.getSimpleName());
    Command command = (Command) registry.lookup(clazz.getSimpleName());
    Future<D> future = executorService.submit(new Worker<D>(command, obj));
    return future.get();
  }

  public void initializeCommands(UserDao user)
      throws RemoteException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

    Reflections reflections = new Reflections("command");
    Set<Class<?>> commandClasses = reflections.getTypesAnnotatedWith(ServerCommand.class);

    for (Class<?> commandClass : commandClasses) {
      Constructor<?> constructor = commandClass.getConstructor(UserDao.class);
      Command command = (Command) constructor.newInstance(user);

//      Remote commandInstance = UnicastRemoteObject.exportObject(command, 2005);
      final WithStubDescriptor commandInstance = skeleton.exportObject(command);

      final ServerCommand annotation = commandClass.getAnnotation(ServerCommand.class);
      String commandName = annotation.name();
      registry.rebind(commandName, commandInstance);

      logger.info("Command {} initialized", commandName);
    }

  }
}
