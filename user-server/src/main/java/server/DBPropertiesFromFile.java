package server;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class DBPropertiesFromFile implements DBProperties {

  private static final Logger LOGGER = LoggerFactory.getLogger(DBPropertiesFromFile.class);

  private Properties props;
  private String jdbcUrl;
  private String login;
  private String passwd;
  private int min;
  private int max;

  public DBPropertiesFromFile(Properties props) {
    this.props = props;
    String jdbcClassName = props.getProperty("jdbc.driver.class");
    jdbcUrl = props.getProperty("db.url");
    login = props.getProperty("db.username");
    passwd = props.getProperty("db.password");
    min = Integer.parseInt(props.getProperty("db.min"));
    max = Integer.parseInt(props.getProperty("db.max"));
  }

  public DBPropertiesFromFile(){
    this(getProperties());
  }

  private static Properties getProperties()  {
    try (InputStream is = App.class.getResourceAsStream("/db.properties")) {
      LOGGER.debug("Initializing User DAO...");
      Properties props = new Properties();
      props.load(is);
      return props;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public String getJdbcUrl() {
    return jdbcUrl;
  }

  @Override
  public String getLogin() {
    return login;
  }

  @Override
  public String getPasswd() {
    return passwd;
  }

  @Override
  public Integer getMin() {
    return min;
  }

  @Override
  public Integer getMax() {
    return max;
  }

}
