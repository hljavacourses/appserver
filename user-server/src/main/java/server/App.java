package server;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;
import command.ExceptionHandlerServerCommandManager;
import command.ServerCommandManager;
import command.ServerCommandManagerImpl;
import dao.user.UserJdbcDao;
import domain.support.jdbc.ConnectionPool;
import domain.support.jdbc.JdbcDaoSupport;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import model.User;
import model.UserTO;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rmi.RmiRegistry;
import rmi.Skeleton;
import rmi.custom.CustomRmiAbstractFactory;
import rmi.custom.WithStubDescriptor;

public class App {

  private static final ExecutorService executorService = Executors.newFixedThreadPool(10);
  private static final Logger logger = LoggerFactory.getLogger(App.class);

  public static Long currentUserId = 0L;
  public static List<UserTO> users;

  static {
    users = new ArrayList<>();
    User user = new User();
    user.setId(++currentUserId);
    user.setName("Pavel");
    user.setPasswd("MyNewPasswordIs******");
    users.add(user);

    LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
    StatusPrinter.print(lc);
  }

  public static void main(String[] args) throws IOException {

    logger.info("Start application server...");

    CustomRmiAbstractFactory rmiFactory = new CustomRmiAbstractFactory();
    RmiRegistry registry = rmiFactory.getRegistry();
    Skeleton skeleton = rmiFactory.createSkeleton();

    DBProperties DBProperties = new WithFallbackDbProperties(
        new EnvDBProperties(),
        new DBPropertiesFromFile());
    PGSimpleDataSource ds = new PGSimpleDataSource();
    ds.setURL(DBProperties.getJdbcUrl());
    ds.setUser(DBProperties.getLogin());
    ds.setPassword(DBProperties.getPasswd());
    UserJdbcDao userDao = new UserJdbcDao(
        new JdbcDaoSupport(
            new ConnectionPool(
                DBProperties.getMin(),
                DBProperties.getMax(), ds)));

    logger.debug("Creating server manager...");
    ExceptionHandlerServerCommandManager scm = new ExceptionHandlerServerCommandManager(
        new ServerCommandManagerImpl(registry, skeleton, executorService));

    try {
      logger.debug("Initializing commands...");
      ((ServerCommandManagerImpl) scm.getOrigin()).initializeCommands(userDao);
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
      logger
          .error("Exception occured while command initializing {}", e.getMessage());
    }

    final WithStubDescriptor remoteServerCommandManager = skeleton.exportObject(scm);
    registry.rebind(ServerCommandManager.class.getName(), remoteServerCommandManager);

    new Thread(() -> {
      try {
        skeleton.start();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }).start();
//    new Thread(() -> {
//      try {
//        registry.start();
//      } catch (Exception e) {
//        e.printStackTrace();
//      }
//    }).start();

    logger.info("Server has been started successfully on port 2006");
  }

}
