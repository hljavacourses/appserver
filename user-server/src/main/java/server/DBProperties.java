package server;

public interface DBProperties {

  String getJdbcUrl();

  String getLogin();

  String getPasswd();

  Integer getMin();

  Integer getMax();
}
