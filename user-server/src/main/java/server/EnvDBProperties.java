package server;

import static env.EnvironmentUtils.envInt;

public class EnvDBProperties implements DBProperties {

  @Override
  public String getJdbcUrl() {
    return System.getenv("jdbc.url");
  }

  @Override
  public String getLogin() {
    return System.getenv("db.login");
  }

  @Override
  public String getPasswd() {
    return System.getenv("db.password");
  }

  @Override
  public Integer getMin() {
    return envInt("db.connections.min", null);
  }

  @Override
  public Integer getMax() {
    return envInt("db.connections.max", null);
  }
}
