package server;

import java.util.function.Function;

public class WithFallbackDbProperties implements DBProperties {

  private DBProperties origin;
  private DBProperties fallback;

  public WithFallbackDbProperties(DBProperties origin, DBProperties fallback) {
    this.origin = origin;
    this.fallback = fallback;
  }

  private <R> R withFallback(Function<DBProperties, R> extractor) {
    final R value = extractor.apply(origin);
    return value == null ? extractor.apply(fallback) : value;
  }

  @Override
  public String getJdbcUrl() {
    return withFallback(DBProperties::getJdbcUrl);
  }

  @Override
  public String getLogin() {
    return withFallback(DBProperties::getLogin);
  }

  @Override
  public String getPasswd() {
    return withFallback(DBProperties::getPasswd);
  }

  @Override
  public Integer getMin() {
    return withFallback(DBProperties::getMin);
  }

  @Override
  public Integer getMax() {
    return withFallback(DBProperties::getMax);
  }
}
