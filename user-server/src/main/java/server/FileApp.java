package server;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ResourceHandler;

public class FileApp {

  private Server server;


  public void start() throws Exception {
    server = new Server();
    ServerConnector connector = new ServerConnector(server);
    connector.setPort(8090);
    server.setConnectors(new Connector[]{connector});
    ResourceHandler staticResourceHandler = new ResourceHandler();
    staticResourceHandler.setResourceBase("C:\\Users\\Brutski_D\\dev\\DesignPatterns\\20.01\\20.01\\rmi-project\\user-server\\target\\classes");
    ContextHandler handler = new ContextHandler("/");
    handler.setHandler(staticResourceHandler);
    server.setHandler(handler);
  }

  public static void main(String[] args) throws Exception {
    new Server().start();
  }

}
