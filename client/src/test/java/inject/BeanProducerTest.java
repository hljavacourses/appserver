package inject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import javax.inject.Inject;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import rmi.RmiRegistry;

public class BeanProducerTest {

  @Mock
  ApplicationContext ctx;
  @Mock
  RmiRegistry registry;
  @InjectMocks
  BeanProducer cut;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void shouldThrowUnsatisfiedDependency() {
    final Throwable actual = Assertions.catchThrowable(() -> cut.produce(BeanA.class));
    assertNotNull(actual);
  }
//
//  @Test
//  public void shouldThrowUnsatisfiedDependency() {
//    when(ctx.getBeansOfType(any())).thenReturn(Collections.singletonMap("DeaA", new DepA()));
//    final BeanA actual = cut.produce(BeanA.class);
//    assertNotNull(actual);
//    assertNotNull(actual.field);
//    assertEquals(DepA.class, actual.field.getClass());
//  }

  @Test
  public void shouldInjectFromContext() {
    when(ctx.getBean(any(Class.class))).thenReturn(new DepA());
    final BeanA actual = cut.produce(BeanA.class);
    assertNotNull(actual);
    assertNotNull(actual.field);
    assertEquals(DepA.class, actual.field.getClass());
  }

  @Test
  public void shouldInjectThroughConstructor() {
    when(ctx.getBean(any(Class.class))).thenReturn(new DepA());
    final BeanB actual = cut.produce(BeanB.class);
    assertNotNull(actual);
    assertNotNull(actual.field);
    assertEquals(DepA.class, actual.field.getClass());
  }


  @Ignore("Build fails with this test")
  @Test
  public void shouldInjectColumn() throws RemoteException, NotBoundException {
    when(registry.lookup(any(),any())).thenReturn(new DepA());
    final BeanC actual = cut.produce(BeanC.class);
    assertNotNull(actual);
    assertNotNull(actual.field);
    assertEquals(DepA.class, actual.field.getClass());
  }

  public static class BeanA {

    @Inject
    DepA field;
  }

  public static class BeanB {

    DepA field;

    @Inject
    public BeanB(DepA field) {
      this.field = field;
    }
  }

  public static class DepA implements Remote {

  }
}