package domain.service;

import command.CreateUserCommand;
import command.GetUsersCommand;
import command.RemoveUserCommand;
import domain.support.CommandManager;
import inject.Command;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Random;
import javax.inject.Inject;
import model.GetUsersTO;
import model.TransferObject;
import model.User;
import model.UserTO;

public class UserServiceImpl implements UserService {

  private static Long currentId = 1L;

  @Command(name = "GetUsersCommand")
  private GetUsersCommand getUsersCommand;

  private CommandManager commandManager;

  private Random random;

  @Command(name = "RemoveUserCommand")
  private RemoveUserCommand removeUserCommand;

  @Command(name = "CreateUserCommand")
  private CreateUserCommand createUserCommand;

  @Inject
  public UserServiceImpl(CommandManager commandManager, Random random) {
    this.commandManager = commandManager;
    this.random = random;
  }

  @Override
  public List<UserTO> getUsers() {
    return executeCommand(getUsersCommand, new GetUsersTO()).getUsers();
  }

  private <T extends TransferObject> T executeCommand(command.Command<T> command, T to) {
    try {
      return command.execute(to);
    } catch (RemoteException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void removeUser(final Long id) {
    UserTO removedUser = executeCommand(removeUserCommand, new User(id));
    System.out.println(removedUser);
  }

  @Override
  public UserTO updateUser(UserTO user) {
    if (user.getName() == null) {
      user.setName(String.format("UserName%02d", random.nextInt(99)));
      user.setPasswd(String.format("UserPasswd%04d", random.nextInt(9999)));
    }
    return executeCommand(createUserCommand, user);
  }
}
