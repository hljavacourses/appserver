package domain.support;

import command.ServerCommandManager;
import exceptions.ProcessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import model.TransferObject;
import rmi.RmiRegistry;

public class CommandManager {

  private final RmiRegistry registry;

  public CommandManager(final RmiRegistry registry) {
    this.registry = registry;
  }

  public <T, D extends TransferObject> D startCommand(final Class<T> clazz, final D obj) {
    try {
      ServerCommandManager scm = getServerCommandManager();
      D result = scm.execute(clazz, obj);
      return result;
    } catch (Exception e) {
      throw new ProcessException(e);
    }
  }

  private ServerCommandManager getServerCommandManager() throws RemoteException, NotBoundException {
    return (ServerCommandManager) this.registry.lookup(ServerCommandManager.class.getName());
  }
}
