package inject;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import javax.inject.Inject;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import rmi.RmiRegistry;

public class BeanProducer implements ApplicationContextAware {

  ApplicationContext ctx;
  private RmiRegistry registry;

  public BeanProducer(RmiRegistry registry) {
    this.registry = registry;
  }

  BeanProducer(ApplicationContext ctx, RmiRegistry registry) {
    this.ctx = ctx;
    this.registry = registry;
  }

  public <T> T produce(Class<T> bean) {
    final T result = newInstance(bean);
    injectFields(bean, result);
    injectColumnFields(bean, result);
    return result;
  }

  private <T> void injectFields(Class<T> bean, T result) {
    for (Field field : bean.getDeclaredFields()) {
      if (field.getAnnotation(Inject.class) != null) {
        try {
          final Object toInject = ctx.getBean(field.getType());
          if (toInject != null) {
            field.setAccessible(true);
            field.set(result, toInject);
          } else {
            throw new IllegalStateException("Unsatisfied dependency");
          }
        } catch (IllegalAccessException e) {
          throw new IllegalStateException("Unavailable @Inject field");
        }
      }
    }
  }

  private <T> void injectColumnFields(Class<T> bean, T result) {
    for (Field field : bean.getDeclaredFields()) {
      final Command command = field.getAnnotation(Command.class);
      if (command != null) {
        try {
          String name = command.name();
          if ("".equals(name)) {
            name = field.getType().getSimpleName();
          }
          final Object toInject = registry.lookup(name, field.getType());
          field.setAccessible(true);
          field.set(result, toInject);
        } catch (IllegalAccessException e) {
          throw new IllegalStateException("Unavailable @Inject field", e);
        } catch (NullPointerException e) {
          throw new IllegalStateException("Bean injection is missing", e);
        } catch (RuntimeException e) {
          throw new IllegalStateException("Unsatisfied dependency", e);
        }
      }
    }
  }

  /**
   * Create new instance via @Inject constructor or default constructor
   */
  private <T> T newInstance(Class<T> bean) {
    Constructor<?> defaultConstructor = null;
    Constructor<?> injectConstructor = null;
    for (Constructor<?> constructor : bean.getConstructors()) {
      if (constructor.getParameterCount() == 0) {
        defaultConstructor = constructor;
      }
      if (constructor.getAnnotation(Inject.class) != null) {
        if (injectConstructor != null) {
          throw new IllegalStateException("more than one @Inject constructors");
        }
        injectConstructor = constructor;
      }
    }
    if (injectConstructor != null) {
      final Object[] parameters = Arrays.stream(injectConstructor.getParameters())
          .map(Parameter::getType).map(ctx::getBean).toArray();
      try {
        return (T) injectConstructor.newInstance(parameters);
      } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
        throw new IllegalStateException(e);
      }
    }
    if (defaultConstructor != null) {
      try {
        return (T) defaultConstructor.newInstance();
      } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
        throw new IllegalStateException(e);
      }
    } else {
      throw new IllegalStateException("No constructors available");
    }
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    ctx = applicationContext;
  }
}
