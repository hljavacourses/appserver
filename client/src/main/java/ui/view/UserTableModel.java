package ui.view;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.User;
import ui.controller.UserController;

public class UserTableModel extends AbstractTableModel {

  List<String> columns;
  List<User> usersList;
  private UserController controller;

  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return false;
  }

  @Override
  public int getColumnCount() {
    return columns.size();
  }

  @Override
  public String getColumnName(int column) {
    return columns.get(column);
  }

  @Override
  public int getRowCount() {
    return usersList.size();
  }

  @Override
  public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
    User user = usersList.get(rowIndex);
    switch (columnIndex) {
      case 0:
        user.setId((Long) value);
        break;
      case 1:
        user.setName(value.toString());
        break;
      case 2:
        user.setPasswd(value.toString());
        break;
    }
    controller.updateUser(user);
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    User user = usersList.get(rowIndex);
    Object value = null;
    switch (columnIndex) {
      case 0:
        value = user.getId();
        break;
      case 1:
        value = user.getName();
        break;
      case 2:
        value = user.getPasswd();
        break;
    }
    return value;
  }

  public void setColumns(List<String> columns) {
    this.columns = columns;
  }

  public void setUsersList(List<User> usersList) {
    this.usersList = usersList;
  }

  public void setController(UserController controller) {
    this.controller = controller;
  }
}
