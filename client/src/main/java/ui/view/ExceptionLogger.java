package ui.view;

public interface ExceptionLogger {
  void appendError(String message);
}
