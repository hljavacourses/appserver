package ui.view;

import javax.swing.JTextArea;

public class TextAreaExceptionLogger extends JTextArea implements ExceptionLogger {

  @Override
  public void appendError(String message) {
    append(message);
    append("\n");
  }
}
