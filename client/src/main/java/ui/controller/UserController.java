package ui.controller;

import domain.service.UserService;
import java.util.List;
import model.User;
import model.UserTO;

public class UserController {

  private UserService srv;

  public UserController(UserService srv) {
    this.srv = srv;
  }

  public UserTO updateUser(final UserTO user) {
    return srv.updateUser(user == null ? new User() : user);
  }

  public void removeUser(final Long id) {
    srv.removeUser(id);
  }

  public List<UserTO> getUsers() {
    return srv.getUsers();
  }
}
