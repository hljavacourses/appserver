package ui.action;

import java.awt.event.ActionEvent;
import java.util.List;
import model.User;
import model.UserTO;
import ui.controller.UserController;
import ui.view.button.ListTableActionListener;

public class AddNewButtonActionListener extends ListTableActionListener {

  private UserController controller;
  private List<UserTO> userList;

  public AddNewButtonActionListener(UserController controller, List<UserTO> userList) {
    this.controller = controller;
    this.userList = userList;
  }

  public void actionPerformed(ActionEvent e) {
    UserTO user = controller.updateUser(null);
    userList.add(user);
    table.revalidate();
  }
}
