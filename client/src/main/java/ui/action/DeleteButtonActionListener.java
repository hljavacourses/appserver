package ui.action;

import java.awt.event.ActionEvent;
import java.util.List;
import model.User;
import ui.controller.UserController;
import ui.view.button.ListTableActionListener;


public class DeleteButtonActionListener extends ListTableActionListener {

  private UserController controller;
  private List<User> userList;

  public DeleteButtonActionListener(UserController controller, List<User> userList) {
    this.controller = controller;
    this.userList = userList;
  }

  public void actionPerformed(ActionEvent e) {
    if (table.getSelectedRow() == -1) {
      return;
    }
    Long id = (Long) table.getValueAt(table.getSelectedRow(), 0);
    controller.removeUser(id);
    userList.remove(table.getSelectedRow());
    table.revalidate();
  }
}