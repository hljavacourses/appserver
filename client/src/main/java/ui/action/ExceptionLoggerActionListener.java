package ui.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import ui.view.ExceptionLogger;

public class ExceptionLoggerActionListener implements ActionListener {

  ActionListener listener;
  ExceptionLogger logger;

  public ExceptionLoggerActionListener(ActionListener listener, ExceptionLogger logger) {
    this.listener = listener;
    this.logger = logger;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    try {
      listener.actionPerformed(e);
    } catch (Throwable ex) {
      ex.printStackTrace();
      logger.appendError(ex.getLocalizedMessage());
    }
  }
}
