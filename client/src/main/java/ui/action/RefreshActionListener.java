package ui.action;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.JTable;
import model.UserTO;
import ui.controller.UserController;
import ui.view.button.ListTableActionListener;

public class RefreshActionListener extends ListTableActionListener {

  private final UserController controller;
  private final List<UserTO> usersList;

  public RefreshActionListener(UserController controller, List<UserTO> usersList, JTable table) {
    this.controller = controller;
    this.usersList = usersList;
    this.table = table;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    List<UserTO> users = controller.getUsers();
    usersList.clear();
    usersList.addAll(users);
    table.revalidate();
  }
}
