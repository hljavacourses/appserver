package com.iba.hlteam.context;

import static org.junit.Assert.assertEquals;

import com.iba.hlteam.context.util.CustomNameParser;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NameParser;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import org.junit.Test;

public class CustomContextTest {

  @Test
  public void runCustomContext() throws NamingException {

    InitialContextFactory factory = new CustomFactory();
    Context initialContext = factory.getInitialContext(new Hashtable());
    NameParser parser = new CustomNameParser();

    initialContext.bind(parser.parse("test_key"), "test_value");

    Object value = initialContext.lookup(parser.parse("test_key"));

    assertEquals((String)value, "test_value");
  }

}
