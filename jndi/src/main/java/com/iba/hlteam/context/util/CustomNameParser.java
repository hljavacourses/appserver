package com.iba.hlteam.context.util;

import java.util.Properties;
import javax.naming.CompoundName;
import javax.naming.Name;
import javax.naming.NameParser;
import javax.naming.NamingException;

public class CustomNameParser implements javax.naming.NameParser {
  static Properties syntax = new Properties();
  static {
    syntax.put("jndi.syntax.direction", "flat");
    syntax.put("jndi.syntax.ignorecase", "false");
  }
  public Name parse(String name) throws NamingException {
    return new CompoundName(name, syntax);
  }
}
