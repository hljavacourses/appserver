package com.iba.hlteam.context;

import java.util.Enumeration;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

public class CustomBindings<T> implements NamingEnumeration {

  Enumeration names;

  public CustomBindings(Enumeration names) {
    this.names = names;
  }

  public Object next() throws NamingException {
    return names.nextElement();
  }

  public boolean hasMore() throws NamingException {
    return names.hasMoreElements();
  }

  public void close() throws NamingException {
    names = null;
  }

  public boolean hasMoreElements() {
    return names.hasMoreElements();
  }

  public Object nextElement() {
    return names.nextElement();
  }
}
