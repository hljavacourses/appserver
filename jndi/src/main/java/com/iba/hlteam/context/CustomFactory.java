package com.iba.hlteam.context;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;

public class CustomFactory implements InitialContextFactory {

  public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
    return new CustomContext(environment);
  }
}
