package com.iba.hlteam.context;

import java.util.Enumeration;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

public class CustomNamingEnumeration<T> implements NamingEnumeration {
  Enumeration names;

  CustomNamingEnumeration(Enumeration names) {
    this.names = names;
  }

  public boolean hasMoreElements() {
    return names.hasMoreElements();
  }

  public boolean hasMore() throws NamingException {
    return hasMoreElements();
  }

  public Object nextElement() {
    if (!names.hasMoreElements()) {
      return null;
    }
    return names.nextElement();
  }

  public Object next() throws NamingException {
    return nextElement();
  }

  public void close() {
  }
}
