package register;

import env.EnvironmentUtils;
import rmi.custom.CustomRmiAbstractFactory;
import rmi.custom.Registry;

public class CustomServerRegister {

  public static void main(String[] args) {
    final CustomRmiAbstractFactory factory = new CustomRmiAbstractFactory(
        EnvironmentUtils.envInt("skeleton.port", 5010),
        EnvironmentUtils.envInt("registry.port", 5011));
    Registry registry = factory.createRegistry();
    registry.start();
  }

}
