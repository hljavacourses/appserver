package jms;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JmsMqConnectionFactoryTest {

    @Ignore("Run IBM MQ before starting test")
    @Test
    public void shouldSendAndReturnMessaageFromQueue_whenSendAndGetMessage_givenMessageToSend() throws Exception {
        // given
        String givenMessage = "Hello world!";

        String expectedMessage = "Hello world!";

        // when
        JmsMqConnectionFactory jmsMqConnectionFactory = new JmsMqConnectionFactory("192.168.99.100",
                1414,
                "PASSWORD.SVRCONN",
                "QM1",
                "sci",
                "sci");
        JmsMessaging messaging = jmsMqConnectionFactory.getJmsMqConnection("DEV.QUEUE.1");
        messaging.send(givenMessage);

        String actualMessage = messaging.get();

        // then
        assertEquals(actualMessage, expectedMessage);
    }

}
