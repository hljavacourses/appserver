package jms;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;

public class JmsMqConnection implements JmsMessaging, AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(JmsMqConnection.class);

    private Session session;

    private MessageProducer producer;
    private MessageConsumer consumer;

    JmsMqConnection(Session session, String queueName) throws JMSException {
        this.session = session;
        Destination destination = session.createQueue("queue:///" + queueName);
        producer = session.createProducer(destination);
        consumer = session.createConsumer(destination);
    }

    /**
     * Sends message to a proper queue
     */
    @Override
    public void send(String msg) throws JMSException {
        producer.send(session.createTextMessage(msg));
        LOGGER.info("Message sent");
    }

    /**
     * Gets message from a proper queue
     */
    @Override
    public String get() throws JMSException {
        Message receivedMessage = consumer.receive();
        LOGGER.info("Message got");
        return receivedMessage.getBody(String.class);
    }

    @Override
    public void close() throws Exception {
        if (session != null) {
            session.close();
        }
    }

}
