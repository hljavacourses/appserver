package jms;

import javax.jms.JMSException;

public interface JmsMessaging {
    void send(String msg) throws JMSException;
    String get() throws JMSException;
}
