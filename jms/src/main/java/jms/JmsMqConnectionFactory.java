package jms;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Session;

import static com.ibm.msg.client.wmq.WMQConstants.*;

/**
 * Class for working with JMS (IBM MQ)
 */
public class JmsMqConnectionFactory implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(JmsMqConnectionFactory.class);

    private Connection connection;

    public JmsMqConnectionFactory(String host, int port, String channel, String queueManager, String username, String password) throws JMSException {
        // create a connection factory
        JmsFactoryFactory ff = JmsFactoryFactory.getInstance(WMQ_PROVIDER);
        JmsConnectionFactory jmsConnectionFactory = ff.createConnectionFactory();

        // set the properties
        jmsConnectionFactory.setStringProperty(WMQ_HOST_NAME, host);
        jmsConnectionFactory.setIntProperty(WMQ_PORT, port);
        jmsConnectionFactory.setStringProperty(WMQ_CHANNEL, channel);
        jmsConnectionFactory.setIntProperty(WMQ_CONNECTION_MODE, WMQ_CM_CLIENT);
        jmsConnectionFactory.setStringProperty(WMQ_QUEUE_MANAGER, queueManager);
        jmsConnectionFactory.setStringProperty(WMQ_APPLICATIONNAME, "JMS");
        jmsConnectionFactory.setBooleanProperty(USER_AUTHENTICATION_MQCSP, true);
        jmsConnectionFactory.setStringProperty(USERID, username);
        jmsConnectionFactory.setStringProperty(PASSWORD, password);

        this.connection = jmsConnectionFactory.createConnection();
        connection.start();

        LOGGER.info("JMS connection factory configured");
    }

    public JmsMqConnection getJmsMqConnection(String queueName) throws JMSException {
        // TODO: JNDI can be used to search for the destination {@see https://developer.ibm.com/messaging/learn-mq/mq-tutorials/develop-mq-jms/}
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        return new JmsMqConnection(session, queueName);
    }

    public static void main(String[] args) {
        // FIXME: If docker is used, system properties can be used to create JMS factory (example: main method in registry module)
    }

    @Override
    public void close() throws Exception {
        if (connection != null) {
            connection.close();
        }
    }

}
