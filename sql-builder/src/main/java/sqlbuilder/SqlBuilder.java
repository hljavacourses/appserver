package sqlbuilder;

import static java.util.stream.Collectors.joining;

import model.Entity;

public class SqlBuilder {

  private static final String SQL_INSERT = "INSERT INTO %s.%s (%s) VALUES (%s)";
  private static final String SQL_DELETE = "DELETE FROM %s.%s WHERE id = ?";
  private static final String SQL_NEXT_ID = "SELECT nextval('%s.%s_id_seq'::regclass)";
  private static final String SQL_SELECT_BY_ID = "SELECT %s FROM %s.%s WHERE id = ?";
  private static final String SQL_SELECT = "SELECT %s FROM %s.%s";

  private final String schema = "rmi";

  public <T extends Entity> String getInsertSQL(final T obj) {
    EntityDescriptor descr = new EntityDescriptor(obj.getClass());
    String columns = String.join(", ", descr.columnsWithoutId());
    String values = descr.columnsWithoutId().stream().map(c -> "?").collect(joining(", "));
    String tableName = descr.tableName();
    return String.format(SQL_INSERT, schema, tableName, columns, values);
  }

  public String getNextIdSQL(final Class<? extends Entity> clazz) {
    EntityDescriptor descr = new EntityDescriptor(clazz);
    return String.format(SQL_NEXT_ID, schema, descr.tableName());
  }

  public String getSelectByIdSQL(final Class<? extends Entity> clazz) {
    EntityDescriptor descr = new EntityDescriptor(clazz);
    return String.format(SQL_SELECT_BY_ID, String.join(", ", descr.columnsWithoutId()), schema,
        descr.tableName());
  }

  public String getSelectSQL(final Class<? extends Entity> clazz) {
    EntityDescriptor descr = new EntityDescriptor(clazz);
    return String.format(SQL_SELECT, String.join(", ", descr.columns()), schema, descr.tableName());
  }

  public String getDeleteSQL(final Class<? extends Entity> clazz) {
    EntityDescriptor descr = new EntityDescriptor(clazz);
    return String.format(SQL_DELETE, schema, descr.tableName());
  }

  public String getUpdateSQL(final Class<? extends Entity> clazz) {
    EntityDescriptor descr = new EntityDescriptor(clazz);
    final String values = descr.columns()
        .stream()
        .map(x -> x + " = ?")
        .collect(joining(", "));
    final String key = descr.idName() + "= ?";
    return String.format("UPDATE %s.%s SET %s WHERE %s", schema, descr.tableName(), values, key);
  }
}
