package sqlbuilder;

import static java.util.stream.Collectors.toList;

import exceptions.ProcessException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Stream;
import model.Entity;

public class EntityDescriptor {

  private Class<? extends Entity> clazz;

  public EntityDescriptor(Class<? extends Entity> clazz) {
    this.clazz = clazz;
  }

  String tableName() {
    return clazz.getSimpleName();
  }

  public String idName() {
    return "id";
  }

  public List<String> columns() {
    return Stream
        .concat(Stream.of("id"), Arrays.stream(clazz.getDeclaredFields()).map(Field::getName))
        .collect(toList());
  }

  List<String> columnsWithoutId() {
    return Arrays.stream(clazz.getDeclaredFields()).map(Field::getName)
        .filter(f -> !idName().equals(f)).collect(toList());
  }

  public <T extends Entity> List<Object> values(T entity) {
    return columns().stream().map(name -> propertyValue(name, entity)).collect(toList());
  }

  private <T extends Entity> Object propertyValue(String name, T entity) {
    try {
      final String getter = getter(name);
      final Method m;
      m = clazz.getMethod(getter);
      return m.invoke(entity);
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      throw new ProcessException(e);
    }
  }

  private String getter(String property) {
    return "get" + property.substring(0, 1).toUpperCase(Locale.ENGLISH) + property.substring(1);
  }

  public <T extends Entity> List<Object> valuesWithoutId(T entity) {
    return Arrays.stream(clazz.getDeclaredFields())
        .map(Field::getName)
        .map(x -> propertyValue(x, entity))
        .collect(toList());
  }

  public <T extends Entity> T newInstance() {
    try {
      for (Constructor<?> ctr : clazz.getConstructors()) {
        if (ctr.getParameterCount() == 0) {
          return (T) ctr.newInstance();
        }
      }
      throw new IllegalStateException(
          "No default constructor for class " + clazz.getCanonicalName());
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      throw new RuntimeException(e);
    }
  }

  public <T extends Entity> T setValues(T entity, ResultSet rs) {
    try {
      for (String column : columns()) {
        Object value = rs.getObject(column);
        Field field = getField(column, clazz);
        field.setAccessible(true);
        field.set(entity, value);
      }
      return entity;
    } catch (SQLException | IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }

  private Field getField(String column, Class<?> clazz) {
    try {
      Objects.requireNonNull(clazz);
      return clazz.getDeclaredField(column);
    } catch (NoSuchFieldException e) {
      return getField(column, clazz.getSuperclass());
    }
  }

  public <T extends Entity> Object id(T entity) {
    return propertyValue(idName(), entity);
  }
}
