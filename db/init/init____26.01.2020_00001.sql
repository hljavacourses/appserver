create schema rmi;

create table rmi."user"
(
    id     bigserial    not null
        constraint user_pk
            primary key,
    name   varchar(255) not null,
    passwd varchar(255) not null
);

alter table rmi."user"
    owner to postgres;

create unique index user_id_uindex
    on rmi."user" (id);

