Жава программа запакованная в app.jar запускается командой:

```
java -jar app.jar
```

Структура файловой структуры Docker контейнера с исполняемым жава архивом:

```
/
+-- app.jar - исполняемый архив
+-- lib/ - зависимости нужны для работы приложения
```

Для работы приложения в app.jar должны быть указаны Main-Class и место [расположение зависимостей](https://docs.oracle.com/javase/tutorial/essential/environment/paths.html). 
Эти значения задаются в файле `/META-INF/MANIFEST.MF` в корне архива.

``` 
# /META-INF/MANIFEST.MF
Class-Path: lib/*.jar
Main-Class: register.CustomServerRegister
```
