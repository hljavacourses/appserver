package com.example;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Setter;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;
import domain.service.UserService;
import java.util.List;
import java.util.Objects;
import model.User;
import model.UserTO;
import org.springframework.beans.factory.annotation.Autowired;

@Route
@PWA(name = "My TODO App", shortName = "My TODO")
@Push
public class MainView extends HorizontalLayout implements BeforeEnterObserver {

  @Autowired
  private UserService service;
  private Binder<UserTO> binder = new Binder<>();

  public MainView() {
    add(createDetailsTab(), createListTab());
  }

  private VerticalLayout createListTab() {
    Grid<UserTO> users = new Grid<>(UserTO.class);
    users.setMaxWidth("500px");
    Button readButton = new Button("Read");
    readButton.addClickListener(event -> {
      final List<UserTO> list = service.getUsers();
      users.setItems(list);
    });

    return new VerticalLayout(users, readButton);
  }

  private VerticalLayout createDetailsTab() {

    // labels
    Label nameLabel = new Label("Customer Name");
    Label idLabel = new Label("Customer ID");
    Label adressLabel = new Label("Customer Adress");

    // text fields
    TextField nameField = new TextField();
    IntegerField idField = new IntegerField();
    TextField adressField = new TextField();
    binder.bind(nameField, UserTO::getName, UserTO::setName);
    binder.bind(idField, composeNullable(UserTO::getId, Long::intValue),
        composeNullable(UserTO::setId, Integer::longValue));
    binder.bind(adressField, UserTO::getPasswd, UserTO::setPasswd);

    // fields
    HorizontalLayout customerName = new HorizontalLayout(nameLabel, nameField);
    HorizontalLayout customerId = new HorizontalLayout(idLabel, idField);
    HorizontalLayout customerAdress = new HorizontalLayout(adressLabel, adressField);
    VerticalLayout fields = new VerticalLayout(customerName, customerId, customerAdress);
    fields.setJustifyContentMode(JustifyContentMode.CENTER);
    fields.setSizeFull();

    TextArea log = new TextArea();
    log.setWidth("350px");
    log.setReadOnly(true);

    // buttons
    Button addButton = new Button("Add Customer");
    addButton.addClickListener(this::createUserListener);

    Button updateButton = new Button("Update Customer");
    updateButton.addClickListener(this::updateUserListener);

    Button deleteButton = new Button("Delete Customer");
    deleteButton.addClickListener(this::deleteCustomerListener);

    final Button getCustomerButton = getCustomerButton();

    HorizontalLayout firstButtonRow = new HorizontalLayout(getCustomerButton, addButton);
    HorizontalLayout secondButtonRow = new HorizontalLayout(updateButton, deleteButton);
    VerticalLayout buttons = new VerticalLayout(firstButtonRow, secondButtonRow);

    return new VerticalLayout(fields, buttons, log);
  }

  private void createUserListener(ClickEvent<Button> buttonClickEvent) {
    UserTO user = new User();
    if (binder.writeBeanIfValid(user)) {
      user.setId(null);
      user = service.updateUser(user);
      binder.readBean(user);
    } else {
      throw new RuntimeException("invalid user");
    }
  }

  @Override
  public void beforeEnter(BeforeEnterEvent event) {

    if (!"passed".equals(event.getUI().getSession().getAttribute("auth"))) {
      event.forwardTo("login");
    }

  }

  private void updateUserListener(
      ClickEvent<Button> x) {
    UserTO user = new User();
    if (binder.writeBeanIfValid(user)) {
      user = service.updateUser(user);
      binder.readBean(user);
    } else {
      throw new RuntimeException("invalid user");
    }
  }

  private Button getCustomerButton() {
    Button button = new Button("Get Customer");
    button.addClickListener(this::getCustomerListener);
    return button;
  }

  private <T, E, R> ValueProvider<T, R> composeNullable(ValueProvider<T, E> first,
      ValueProvider<E, R> second) {
    return x -> {
      E apply = first.apply(x);
      return apply == null ? null : second.apply(apply);
    };
  }

  private <T, E, R> Setter<T, E> composeNullable(Setter<T, R> setter, ValueProvider<E, R> mapper) {
    return (o, v) -> {
      if (v != null) {
        setter.accept(o, mapper.apply(v));
      }
    };
  }

  private void deleteCustomerListener(ClickEvent<Button> x) {
    final User user = new User();
    if (binder.writeBeanIfValid(user)) {
      service.removeUser(user.getId());
      binder.readBean(new User());
    } else {
      throw new RuntimeException("invalid user");
    }
  }

  private void getCustomerListener(ClickEvent<Button> x) {
    final User user = new User();
    if (binder.writeBeanIfValid(user)) {
      service.getUsers()
          .stream()
          .filter(y -> Objects.equals(y.getId(), user.getId()))
          .findFirst()
          .ifPresent(binder::readBean);
    } else {
      throw new RuntimeException("invalid user");
    }
  }
}
