package com.example;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;

@Route("raid")
@PageTitle("Raid")
@Push
public class Raid extends VerticalLayout {
	private static final String TEXT = "Погрузись в захватывающую RPG в духе темного фэнтези, где тебя ждут сотни героев из 16 фракций! "
			+ "Открой для себя Raid: Shadow Legends уже сейчас!";

	public Raid() {
		StreamResource res = new StreamResource("raid.png",
				() -> MainView.class.getClassLoader().getResourceAsStream("raid.png"));
		add(new Image(res, TEXT));
		add(new Text(TEXT));
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);

		Button getButton = new Button("To Main");
		getButton.addClickListener(e -> {
			UI.getCurrent().navigate(MainView.class);
		});
		add(getButton);

		addClickListener(e -> {
			UI.getCurrent().navigate(MainView.class);
		});
	}

}
