package com.example;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;
import command.GetUsersCommand;
import domain.service.UserService;
import domain.service.UserServiceImpl;
import domain.support.CommandManager;
import inject.BeanProducer;
import java.rmi.RemoteException;
import java.util.Random;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import rmi.RmiRegistry;
import rmi.custom.CustomRmiAbstractFactory;

/**
 * The entry point of the Spring Boot application.
 */
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

  static {
    LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
    StatusPrinter.print(lc);
  }

  public static void main(String[] args) throws RemoteException {
    new Application().registry()
        .lookup("GetUsersCommand", GetUsersCommand.class);
    SpringApplication.run(Application.class, args);
  }

  @Bean
  public RmiRegistry registry() throws RemoteException {
    CustomRmiAbstractFactory rmiFactory = new CustomRmiAbstractFactory();
    return rmiFactory.getRegistry();
  }

  @Bean
  public CommandManager commandManager(RmiRegistry registry) {
    return new CommandManager(registry);
  }

  @Bean
  public BeanProducer beanProducer(RmiRegistry registry) {
    return new BeanProducer(registry);
  }

  @Bean
  public UserService userService(BeanProducer beanProducer) {
    return beanProducer.produce(UserServiceImpl.class);
  }

  @Bean
  public Random random(BeanProducer beanProducer) {
    return beanProducer.produce(Random.class);
  }
}
