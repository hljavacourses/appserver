package com.example;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.login.AbstractLogin.LoginEvent;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Tag("sa-login-view")
@Route(value = LoginView.ROUTE)
@PageTitle("Login")
@Push
public class LoginView extends VerticalLayout implements BeforeEnterObserver {
	public static final String ROUTE = "login";
	private static final Logger logger = LoggerFactory.getLogger(LoginView.class);

	private LoginOverlay login; //

	public LoginView() {
		login = new LoginOverlay(); //
		login.addLoginListener(e -> {
			boolean isAuthenticated = authenticate(e);
			if (isAuthenticated) {
				logger.debug("Redirect to page Raid");
				UI.getCurrent().getSession().setAttribute("auth", "passed");
				UI.getCurrent().navigate(Raid.class);
				UI.getCurrent().getPage().reload();
			} else {
				login.setError(true);
			}
		});
		login.setOpened(true);
		login.setTitle("Secured Vaadin");
		getElement().appendChild(login.getElement());
	}

	// пока просто так без коннекта к лдапу
	private boolean authenticate(LoginEvent event) {
		logger.info("User {} has authenticated successfully", event.getUsername());
		return true;
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		if ("passed".equals(event.getUI().getSession().getAttribute("auth"))) {
			UI.getCurrent().navigate(MainView.class);
			UI.getCurrent().getPage().reload();
		}

	}

}