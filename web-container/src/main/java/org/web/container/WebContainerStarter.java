package org.web.container;

import java.io.IOException;

public class WebContainerStarter {

  public static void main(String[] args) throws IOException {
    new WebContainer().start(7070, new HttpHandlerWithServletSupport(new TestServlet()));
  }

}
