package org.web.container.servletsupport;

import java.util.Enumeration;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

public class MyServletConfig implements ServletConfig {

  @Override
  public String getServletName() {
    return null;
  }

  @Override
  public ServletContext getServletContext() {
    return null;
  }

  @Override
  public String getInitParameter(String name) {
    return null;
  }

  @Override
  public Enumeration<String> getInitParameterNames() {
    return null;
  }
}
