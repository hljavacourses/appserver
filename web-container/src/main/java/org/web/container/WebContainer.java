package org.web.container;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebContainer {

  private static final Logger LOGGER = LoggerFactory.getLogger(WebContainer.class);

  private HttpServer server;

  public void start(int port, HttpHandler handler) throws IOException {
    InetSocketAddress inetSocketAddress = new InetSocketAddress(port);
    server = HttpServer.create(inetSocketAddress, 0);
    server.createContext("/", handler);
    LOGGER.info("Http server listen {} address", inetSocketAddress);
    server.start();
  }
}
