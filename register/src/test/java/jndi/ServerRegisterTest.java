package jndi;

import org.junit.Ignore;
import org.junit.Test;

import javax.naming.InitialContext;
import java.util.Hashtable;

import static javax.naming.Context.INITIAL_CONTEXT_FACTORY;
import static javax.naming.Context.PROVIDER_URL;
import static org.junit.Assert.assertEquals;

public class ServerRegisterTest {

    @Test
    @Ignore
    public void shouldReturnStringFromInitialContext_whenStartServerRegisterAndStringBindings_givenStringValueToBind() throws Exception {
        // given
        String givenString = "Hello";
        String key = "value";

        String expectedString = "Hello";

        // when
        ServerRegister server = new ServerRegister();
        server.start(5000);

        Hashtable<String, String> jndiProperties = new Hashtable<>();
        jndiProperties.put(INITIAL_CONTEXT_FACTORY, ServerRegister.CONTEXT_FACTORY_CLASS);
        jndiProperties.put(PROVIDER_URL, server.getURL());

        InitialContext ctx = new InitialContext(jndiProperties);
        ctx.bind(key, givenString);

        String actualString = (String) ctx.lookup(key);

        server.stop();

        // then
        assertEquals(actualString, expectedString);
    }

}
