package jndi;

import static javax.naming.Context.INITIAL_CONTEXT_FACTORY;

import java.net.InetAddress;
import javax.naming.NamingException;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.jnp.server.Main;
import org.jnp.server.NamingBeanImpl;

/**
 * JNDI server for object transfer
 */
public class ServerRegister {

  public static final String CONTEXT_FACTORY_CLASS = "org.jnp.interfaces.NamingContextFactory";
  // Log4J basic configuration
  private static final Logger LOGGER = Logger.getLogger(ServerRegister.class);

  static {
    BasicConfigurator.configure();
  }

  private final NamingBeanImpl namingServer = new NamingBeanImpl();
  private final Main jnpServer = new Main();

  public static void main(String[] args) throws NamingException {
    String port = System.getenv("port");
    if (port == null) {
      port = "5000";
    }
    new ServerRegister().start(Integer.parseInt(port));
  }

  /**
   * @return URL of a started server
   */
  public String getURL() {
    return jnpServer.getBindAddress() + ":" + jnpServer.getPort();
  }

  /**
   * Starts JNDI server
   */
  public void start(int port) {
    System.setProperty(INITIAL_CONTEXT_FACTORY, CONTEXT_FACTORY_CLASS);
    try {
      // FIXME cannot be started
      namingServer.start();

      jnpServer.setNamingInfo(namingServer);
      jnpServer.setBindAddress(InetAddress.getLocalHost().getHostName());
      jnpServer.setPort(port);
      jnpServer.start();

      LOGGER.info("Server started on " + getURL());
    } catch (Exception e) {
      stop();
      throw new RuntimeException("Unexpected exception in JNDI server");
    }
  }

  /**
   * Stops JNDI server
   */
  public void stop() {
    jnpServer.stop();
    namingServer.stop();
  }

}
